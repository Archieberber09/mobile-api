<?php

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Validator;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'fail', 'data' => $validator->errors()], 400);
        }
        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials)) {
            $error = "Unauthorized";
            return response()->json(['status' => 'error', 'message' => $error], 401);
        }
        $user = $request->user();
        $roles = $user->role;
        $mobile_number = $user->mobile_number;
        $points = $user->points;
        $id = $user->id;
        $success['token'] =  $user->createToken('token')->accessToken;
        $success['user'] = ['name' => $user->name, 'roles' => $roles , 'mobile_number' => $mobile_number,'points' => $points, 'id' => $id ];
        return response()->json(['status' => 'success', 'data' => $success], 200);
    }
    public function logout(Request $request)
    {
        $isUser = $request->user()->token()->revoke();
        if ($isUser) {
            $message = "Successfully logged out.";
            return response()->json(['status' => 'success', 'message' => $message], 200);
        } else {
            $error = "Something went wrong.";
            return response()->json(['status' => 'error', 'message' => $error], 401);
        }
    }
    public function signup(Request $request)
    {
        $validator = Validator::make($request->json()->all(),[
            'name' => 'required',
    		'email' => 'required|email',
    		'mobile_number' => 'required',
    		'password' => 'required|min:8'

        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(),400);
        }
        
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = 'customer';
        $user->mobile_number = $request->mobile_number;
        $user->points = 0;
        $user->password = Hash::make($request->password);
        $user->token = Str::random(15);
        $user->save();
        $token = $user->token;
        // $user ->toJson();
        
        return response()->json(compact('user','token'),200);
    }

    public function user(Request $request)
    {
        return response()->json($request->user());
    }
    
}