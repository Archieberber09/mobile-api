<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    public function register(Request $request){
    	$request->validate([
    		'name' => 'required',
    		'email' => 'required|email',
    		'mobile_number' => 'required',
    		'password' => 'required|min:8'
    	]);

    	$isExists = User::whereEmail($request->email)->first();

    	if(!$isExists){
    		$user = new User;
	    	$user->name = $request->name;
	    	$user->email = $request->email;
	    	$user->role = 'customer';
            $user->mobile_number = $request->mobile_number;
	    	$user->points = 0;
	    	$user->password = Hash::make($request->password);
	    	$user->token = Str::random(15);
	    	$user->save();
	    	return response(['user' => $user, 'message' =>  'Registration successful']);
    	}else{
	    	return response(['message' =>  'Email already exists']);
    	}
    }
}
