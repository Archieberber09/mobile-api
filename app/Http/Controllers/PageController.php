<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboard()
    {
        $users = User::all();

        return view('/dashboard/index', compact('users'));
    }
    public function getcollection()
    {
        return view('dashboard/collection');
    }

    public function register()
    {
        return view('/dashboard/registration');
    }

    public function forgotpassword()
    {
        return view('/auth/forgotpassword');
    }
}
