<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForgotPassword extends Mailable
{
    use Queueable, SerializesModels;

    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
       $this->email = $data['email'];
       $this->resetpassword_token = $data['resetpassword_token'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email.forgotpassword')->with([
          'email'    => $this->email,
          'resetpassword_token'    => $this->resetpassword_token
          ]);
    }
}
