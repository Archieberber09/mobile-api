@extends('layouts.app')

@section('dashboard')
    	<div class="h-100 mainpanel">
    		<form method="POST" action="/collect" class="h-100 align-items-center justify-content-center d-flex flex-column">
    			@csrf
    			<img src="/Image/coin.png" style="width: 200px; height: 200px;" class="mb-5">
    			<input type="number" name="weight" class="form-control w-25 mb-3" placeholder="pounds">
    			<button class="btn btn-warning form-control w-25 font-weight-bolder text-primary">COLLECT</button>
    		</form>
    	</div>
@endsection