@extends('layouts.app')

@section('dashboard')
    <div class="h-100 mainpanel">
        <div class="d-flex flex-column justify-content-center align-items-center h-100">
            {!! QrCode:: size(300)->margin(2)->generate(url('api/collect/{{$qrcode->id}}' )); !!}
            <h1>{{$qrcode->points}} Points</h1>
        </div>
    </div>
@endsection