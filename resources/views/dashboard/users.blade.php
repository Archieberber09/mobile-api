@extends('layouts.app')

@section('dashboard')
    <div class="card-header d-flex justify-content-between align-items-center py-4 mb-3">
    	USERS
    	<span>
    		@if (Route::has('register'))
             	<a href="/dashboard/register" class="btn btn-danger">ADD NEW USER +</a></span>
            @endif
    </div>
         <table class="table table-striped table-bordered bg-light text-center">
    	<thead>
    		<tr class="text-center">
    		<th>#</th>
    		<th>NAME</th>
    		<th>EMAIL</th>
    		<th>MOBILE NUMBER</th>
    		<th>ROLE</th>
    		<th>POINTS</th>
    		<th>ACTIONS</th>
    	</tr>
    	</thead>
    	<tbody>
    		@forelse($users as $index => $user)
	    		<tr class="text-center">
	    			<td>{{$index+1}}</td>
	    			<td>{{$user->name}}</td>
	    			<td>{{$user->email}}</td>
	    			<td>{{$user->mobile_number}}</td>
	    			<td>{{$user->role}}</td>
	    			<td>{{$user->points}}</td>
	    			<td class="d-flex justify-content-center">
	    				<form action="/user/{{$user->id}}/edit" action="POST">
	    					<button class="mx-1"><i class="fas fa-edit"></i></button>
	    				</form>
		    			<button class="mx-1" onclick="myfunction({{ $user->id }})"><i class="fas fa-eye"></i></button>
		    			<form action="/user/{{ $user->id }}" method="POST" class="deleteBtn">
							@csrf
							@method("DELETE")
	    					<button class="mx-1"><i class="fas fa-trash"></i></button>
						</form>
	    			</td>
	    		@empty
	    			<td colspan="5">No Available Users</td>
	    		</tr>
    		@endforelse
    	</tbody>
    </table>
    <div class="d-flex"> 
            <div class="mx-auto pagination-users">
                    {!! $users->links(); !!}
            </div>
        </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Total Points: </h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body ">
				<div class="d-flex justify-content-center mb-3">
					<div class="col-md-4 text-right">Name:</div>
					<div class="col-md-8 text-left" id="name"></div>
				</div>
				<div class="d-flex justify-content-center mb-3">
					<div class="col-md-4 text-right">Email:</div>
					<div class="col-md-8 text-left" id="email"></div>
				</div>
				<div class="d-flex justify-content-center mb-3">
					<div class="col-md-4 text-right">Role:</div>
					<div class="col-md-8 text-left" id="role"></div>
				</div>
				<div class="d-flex justify-content-center mb-3">
					<div class="col-md-4 text-right">Mobile Number:</div>
					<div class="col-md-8 text-left" id="mobile_number"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('page-script')
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
	<script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>

	<script>
		//data-tables
		$(document).ready( function () {
			$('.table').DataTable({
				"bPaginate": false,
				"columnDefs": [
				{ "orderable": false, "targets": 5 }
				]
			});
		});

		// user deletion
		document.querySelectorAll(".deleteBtn").forEach(function(id){

			id.addEventListener('submit', e=>{
				
				e.preventDefault()
				Swal.fire({
					title: 'Are you sure?',
					text: "It will permanently deleted !",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Yes, delete it!'
				}).then((result) => {
					if(result.value){
						id.submit();
					}
				})

			})
		})

		//user modal for full details
		function myfunction(id){

			let url = "/getuserdata/" + id;
			fetch(url)
			.then(response=>response.json())
			.then(function(data){
				console.log(data);
				document.querySelector("#name").innerText = data.name;
				document.querySelector("#role").innerText = data.role;
				document.querySelector("#email").innerText = data.email;
				document.querySelector("#mobile_number").innerText = data.mobile_number;
				$("#exampleModal").modal("show");
			});	

			
		}

		// $(document).ready(function(){
		// 	let previous = document.querySelector('.previous');
		// 	previous.style.display = "none"
		// 	previous.nextElementSibling.style.display = "none";
		// 	previous.nextElementSibling.nextElementSibling.style.display = "none";

		// 	document.querySelector('label').style.display = "none";
		// })
	</script>

@endsection