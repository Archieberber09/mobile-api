<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <script src="https://kit.fontawesome.com/3cbbe2888d.js" crossorigin="anonymous"></script>

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/darkly/bootstrap.min.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
    <main>
        @if(Auth::check())
            <div class="container-fluid">
                <div class="row justify-content-center" style="min-height: 100vh;">
                    <div class="col-md-2 bg-dark sidebar pt-5">
                        <ul class="list-group">
                            @if(Auth::user()->role == 'admin')
                                <li class="list-group-item bg-transparent border-0 pt-5"><a href="/dashboard/users" class="text-white">USERS</a></li>
                                <li class="list-group-item bg-transparent border-0"><a href="/dashboard/transactions" class="text-white">TRANSACTIONS</a></li>
                                <li class="list-group-item bg-transparent border-0"><a href="/dashboard/announcements" class="text-white">ANNOUNCEMENT</a></li>
                            @endif
                            @if(Auth::user()->role == 'cashier' || Auth::user()->role == 'admin')
                                <li class="list-group-item bg-transparent border-0"><a href="/dashboard/collections" class="text-white">COLLECTIONS</a></li>
                            @endif
                            <li class="list-group-item bg-transparent border-0"><a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();" class="text-danger">
                                {{ __('LOGOUT') }}
                            </a></li>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </ul>
                    </div>
                    <div class="col-md-10">
                        @yield('dashboard')
                        @include('sweetalert::alert')
                    </div>
                </div>
            </div>
        @else
            @yield('content')
        @endif
    </main>
</div>


<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

@yield('page-script')
</body>

</html>
