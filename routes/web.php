<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	if(Auth::check()){
    	return view('dashboard.index');
	}else{
    	return view('auth.login');
	}
});
Auth::routes();

//Page route
Route::get('/dashboard', 'PageController@dashboard');
Route::get('/dashboard/transactions', 'TransactionController@gettransations');
Route::get('/dashboard/collections', 'PageController@getcollection');
Route::get('/dashboard/register', 'PageController@register');
Route::get('/forgot-password', 'PageController@forgotpassword');

//users route
Route::get('/dashboard/users', 'UserController@getUsers');
Route::post('/register-user', 'UserController@register');
Route::post('/login-user', 'UserController@login');
Route::get('/user/{user}/edit', 'UserController@edit');
Route::patch('/user/{user}/edit', 'UserController@saveupdate');
Route::delete('/user/{user}', 'UserController@destroy');
Route::get('getuserdata/{id}', 'UserController@getuserdata');

//collection route
Route::get('/dashboard/transactions', 'TransactionController@index');
Route::post('/collect', 'TransactionController@create');

//announcement route
Route::get('/dashboard/announcements', 'AnnouncementController@index');
Route::get('/dashboard/announcements/create', 'AnnouncementController@create');
Route::post('/create-announcement', 'AnnouncementController@store');
Route::get('/announcement/{announcement}/edit', 'AnnouncementController@edit');
Route::patch('/announcement/{announcement}/edit', 'AnnouncementController@update');


