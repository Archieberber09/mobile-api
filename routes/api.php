<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// mobile API
Route::post('/login', 'API\LoginController@login');
Route::post('/register', 'API\RegisterController@register');
Route::post('/forgotpassword', 'API\LoginController@forgotpassword');
Route::post('/resetpassword/{id}', 'API\LoginController@resetpassword');
Route::post('/collect/{qrcode}', 'TransactionController@qrscanned');

// // --
Route::get('/user', function(Request $request) {
    return Auth::user();
})->middleware('auth:api')->name('user_get');
Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('/login', 'API\AuthController@login');
    Route::post('/signup', 'API\AuthController@signup');
    Route::get('/userr  ', 'API\AuthController@user');
    
    
    Route::group([
        'middleware' => 'auth:api'
    ], function () {
        Route::get('logout', 'API\AuthController@logout');
        // Route::get('user', 'API\AuthController@user');
        
    });
});